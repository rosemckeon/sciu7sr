#' ---
#' title: "Generalise linear models (GLMs)"
#' desc: "**Stats with R** (SCIU7SR)"
#' author: "**Rose McKeon** (2417024)"
#' date: "November 27th 2018"
#' ---

#+ setup, message=F, warning=F --------
# clear the workspace
rm(list=ls())
# load dependencies
library(tidyverse)
library(broom)
library(knitr)
library(scales)
library(boot)
library(ggplot2)
#library(RColorBrewer)
source("lib/functions.R")

# --------------------------------------
#' ## Data
#' These data include measures of species RICHNESS in plots that have different measures of BIOMASS (a continuous predictor) and three levels of a categorical predictor, pH (PH).
species <- read.csv("data/SPECIES.csv")
str(species)
kable(head(species))
summary(species$PH) # balanced data set
# let's reorder those factors
species$PH <- factor(
  species$PH, levels = c("high","mid","low")
)
str(species)
summary(species$PH)
#----------------------------------------
#' ## Quality control
#' Don't appear to be any outliers.
pairs(species) # collinearity doesn't appear to be an issue.
hist(species$BIOMASS) # predictor: heavily skewed
hist(species$RICHNESS) # response: fairly normal, slight skew
#' Our response (RICHNESS) is an integer (it's count data) and should have reasonable bounds, ie: we should not have predictions with negative numbers, or decimals. A lower bound implies that the error distribution won't be normal, indicating GLMs will be the best approach to model these data.


# ---------------------------------------
#' ## Exploratory plots
qplot(
  BIOMASS, RICHNESS, data = species,
  geom = "point",
  colour = PH
)
#' It doesn't look like there is an interaction as the slopes of the factors appear more or less the same. We'll check anyway. A model without interactions would have 3 coefficients (Intercept, Biomass and  pH) whereas one with an interaction would remove pH and include 4 more (phlow, phmid, Biomass:phlow and Biomass:phmid, Biomassphhigh would be the intercept). All the estimates should be negative apart from the intercept as all the otehr intercepts deviate below that of biomassphhigh.
#'
#' While a linear model would fit this data fairly well, it will be inappropriate because it will make stupid predictions, ie: richness below zero.
#'
#' ### logging the response...
qplot(
  BIOMASS, log(RICHNESS), data = species,
  geom = "point",
  colour = PH
)
#' Plotting the model in loglinear space crudely approximates a GLM and shows us that an interaction might infact exist, slopes now differ.
#'

#-------------------------------------
#' ## Fitting the GLM
fit1 <- glm(
  RICHNESS ~ BIOMASS*PH, data = species,
  # assume the errors have poisson dist for count data
  family = "poisson"
)
glm.diag.plots(fit1)
#include_graphics("assets/fit1-glm-diag")
#' Interactive identification with these diagnostics shows point 18 might be a concern, it's in the warning area of both Cook plots and strays from the line on the QQ.
#'
summary(fit1)
#' **Difference between residual deviance and degrees of freedom (df) falls between 0.5 and 2. This means there is no under / over dispersion and we can trust the P values.**
#'
#' To back transform these values we'd use exp() which is the anti log(), 10^x would be the inverse of log10().
kable(
  exp(fit1$coefficients)
  #col.names = c("Parameter", "Estimate")
)

#-------------------------------
#' ## Model simplification
# remove the interaction
fit2 <- update(fit1, ~. -BIOMASS:PH)
# chi-squared test
anova(fit1, fit2, test = "Chi")
#' We must include the interaction between biomass and pH as removal significantly increases unexplained variation in the model (Deviance = -16.04, Df = 86, P < 0.001).
#'
#' Let's also see if removal of that suspicious point 18 improves our model...
# remove point 18
fit3 <- update(fit1, data = species[-18,])
# check residual deviance changes manually as cannot chi-square two models of different size datasets
summary(fit3)
#' Removal of point 18 does not improve our model, residual deviance decreases causing under dispersion (Residual deviance = 71.977, Df = 83) and AIC is reduced by 16.65 (AIC = 497.74).
#'
#' **final model is original**
fit <- fit1

#--------------------------------------
#' ## Final plot

#' We need to make some predictions to draw our lines. But, we can't use int = c for these models in loglink space and using type = response to get predictions in terms of our original response limits our ability to get real standard errors and confidence limits, so let's do it manually making sure the back transformation happens after the CI calculation from standard error.

# new biomass vals
newx <- seq(
  min(species$BIOMASS), max(species$BIOMASS),
  length = 100
)

# predictions for high
newy_high <- predict(
  fit,
  list(
    PH = factor(rep("high", 100)),
    BIOMASS = newx
  ),
  se = T
)
# str(newy_high)
newy_high$fit.exp <- exp(newy_high$fit)
# lower bound at 95% CI
newy_high$lower <- exp(
  newy_high$fit - 1.96*newy_high$se.fit
)
newy_high$upper <- exp(
  newy_high$fit + 1.96*newy_high$se.fit
)
newy_high <- as.data.frame(newy_high) %>%
  select(fit.exp, lower, upper)
# predictions for mid
newy_mid <- predict(
  fit,
  list(
    PH = factor(rep("mid", 100)),
    BIOMASS = newx
  ),
  se = T
)
newy_mid$fit.exp <- exp(newy_mid$fit)
newy_mid$lower <- exp(
  newy_mid$fit - 1.96*newy_mid$se.fit
)
newy_mid$upper <- exp(
  newy_mid$fit + 1.96*newy_mid$se.fit
)
newy_mid <- as.data.frame(newy_mid) %>%
  select(fit.exp, lower, upper)
# predictions for low
newy_low <- predict(
  fit,
  list(
    PH = factor(rep("low", 100)),
    BIOMASS = newx
  ),
  se = T
)
newy_low$fit.exp <- exp(newy_low$fit)
newy_low$lower <- exp(
  newy_low$fit - 1.96*newy_low$se.fit
)
newy_low$upper <- exp(
  newy_low$fit + 1.96*newy_low$se.fit
)
newy_low <- as.data.frame(newy_low) %>%
  select(fit.exp, lower, upper)

#' Let's use all this to build our plot...

#+ plot, fig.cap="Figure 1. The effects of interacting predictors, biomass and soil pH, on species richness counts. Regression lines show predictions from a generalised linear model (GLM) based on these data. Faint lines surrounding each regression represent the confidence envelope at 95%."
roses_par()
# get a nice colour palette
ph_cols <- hue_pal()(3)
#ph_cols <- hue_pal(c=0, l=c(20,50,80))(3)
# copy ph column and position with ph
species <- add_column(
  species,
  PH.col = species$PH,
  .after = 1
)
# replace factors in copy column with hex colours
levels(species$PH.col)[1] <- ph_cols[1]
levels(species$PH.col)[2] <- ph_cols[2]
levels(species$PH.col)[3] <- ph_cols[3]
summary(species$PH.col)
# base plot
plot(
  RICHNESS ~ BIOMASS, data = species,
  col = alpha(PH.col, .5),
  pch = as.numeric(PH)+14
)
matlines(
  newx, newy_high,
  lty = "solid",
  lwd = 2,
  col = c(
    ph_cols[1],
    alpha(ph_cols[1], .3),
    alpha(ph_cols[1], .3)
  )
)
matlines(
  newx, newy_mid,
  lty = "solid",
  lwd = 2,
  col = c(
    ph_cols[2],
    alpha(ph_cols[2], .3),
    alpha(ph_cols[2], .3)
  )
)
matlines(
  newx, newy_low,
  lty = "solid",
  lwd = 2,
  col = c(
    ph_cols[3],
    alpha(ph_cols[3], .3),
    alpha(ph_cols[3], .3)
  )
)
legend(
  "topright",
  title = "pH",
  legend = c("high", "mid", "low"),
  bty = "n",
  pch = 15:17,
  lty = "solid",
  col = ph_cols
)
#' A genrealised linear model (GLM) was chosen for these count data to properly handle the strict bounds and non-normal error distributions of the response variable, species richness. Modelling revealed that both biomass and soil pH significantly affected species richness and an interaction between these predictors was observed (Biomass by pH interaction: chi-sqaured test, Deviance = -16.04, Df = -2). Species richness was greatest when biomass was low and pH was high, decreasing as biomass increased and pH became more acidic. (see figure 1 and table 1).

#' **no coefficients quoted as I've got confused if they're ok** (see below).

coefs <- tidy(summary(fit)$coefficients)
# add confidence limits
coefs$lower <- coefs$Estimate - 1.96*coefs$Std..Error
coefs$upper <- coefs$Estimate + 1.96*coefs$Std..Error
# back transform
coefs$Estimate <- exp(coefs$Estimate)
coefs$lower <- exp(coefs$lower)
coefs$upper <- exp(coefs$upper)

# output nice table
kable(
  coefs %>% select(
    .rownames, Estimate, lower, upper, z.value, Pr...z..
  ),
  col.names = c(
    "Parameter", "Estimate", "Lower", "Upper", "z", "P"
  ),
  digits = 3,
  caption = "Table 1. Parameter estimates for the best fitting, generalised linear model (GLM), including interaction terms. Intercept represents the factor ph high, other estimates are intercept deviations from ph high. Lower and upper confidence bounds set to 95%, all digits rounded to 3 dp."
)
#' **Is this right? estimates are no longer negative?**
