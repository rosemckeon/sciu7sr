#' # Regression
#' Stats with R /
#' Rose McKeon /
#' Tues October 9th

#' Setup...

#+ setup, message=FALSE, warning=FALSE
# clear the workspace
rm(list=ls())
# load dependencies
library(tidyverse)
source("lib/functions.R")

#' ## The data
#' How the intensity of a sea louse infection in copepods/salmon affects salmon growth (grams of mass gained over experimental period). Infection intensity was tightly controlled to reduce measurement error. Variables contained are: Predictor, `$INFECTION` (int) and response, `$GROWTH` (num).
sealice <- read.csv("data/sealice.csv")

#' ## Quality control
#' There is no particular concerns about the data at this point. Frequency counts of infection categories are broadly distributed, with only a very slight weighting towards 0 infection. Growth is roughly bell-shaped which is adequate considering the small dataset. Intensity and growth, in this case, should remain as a continuous scale.
# check distribution and outliers
hist(sealice$INFECTION)
hist(sealice$GROWTH)

#' ## The relationship
#' A strong negative relationship is observed.
plot(
  GROWTH ~ INFECTION, data = sealice,
  ylim = c(0,8) # to assist intercept estimation
  )
#' ### Slope estimation (using data points)
(1-5)/(12-0)
#' ### Intercept estimation (by eye)
#' 7

#' ## Fitting the model
fit <- lm(GROWTH ~ INFECTION, data = sealice)
# number of residuals - one for each datapoint
length(fit$residuals)

#' ### Checking the fit
#' Overall the fit is pretty good considering the size of our dataset.
#' - Residuals vs fitted vals shows no real pattern. Non-linearity, especially for the small amount of data implies a good fit.
#' - QQ plot shows the theoretical and observed points are similar as they sit fairly well on the line. Implies a good fit.
#' - Scale-Location shows pattern where none should be. Implies fit could be better.
#' - Cook's distance plot shows that point 4 may be having a high influence on our model fit. Given the size of the data this may not be an issue, but it's worth noting that point 4 is also an outlier in the other diagnostic plots. A comparison of the fit with and without point 4 may be worthwhile.
#' - The histogram of residuals shows a pretty good noral distribution for the dataset size.
# adjust plotting panel to 2x3
par(mfrow=c(2,3))
# model diagnostics
plot(fit)
# distribution of residuals
hist(fit$residuals)
# return plotting panel to default 1x1
par(mfrow=c(1,1))
#' The slope and intercept of the fit are similar to our estimates, but less steep.
fit$coefficients

#' ### Model summary
#' The slope is labelled INFECTION in prepartion for the model being able to accept multiple predictors with different slopes. The P-val for INFECTION is the one that is relevant to the relationship we are interested in. **Our P-val is < 0.001** so we can reject the null hypothesis that there is no relationship.
summary(fit)
#' The change in growth expected for every unit of infection is -0.411.
# confidence intervals around that
confint(fit)

#' ## Analysis summary
#' **Infection intensity--number of copepods per salmon--negatively impacted growth rate** (Slope: -0.411, CI: &plusmn;0.13, t: -6.94, P: 2.46x10<sup>-5</sup>).

#' The F-statistic P val is the same as that of our slope because there is only one pedictor in our model. This summary line tests the entire regression, so if we had multiple predictors this value would change.
#'
#' ### Variation
#' The coefficient of determination shows us that **81% of the variation seen in growth can be explained by infection intensity** (R<sup>2</sup>: 0.814).
#'
#' ### Analysis of variance
#' For the Slope (INFECTION):
#'
#' - **Sum Sq** is the sum of squared *explained* distances.
#' - **Mean Sq** is the conservative mean variance *explained*. (Sum Sq/Df)
#'
#' For the Residuals:
#'
#' - **Sum Sq** is the sum of squared *unexplained* distances.
#' - **Mean Sq** is the conservative mean variance *unexplained*. (Sum Sq/Df)
#' Degrees of freedom for our slope is 1 because there is only 1 predictor in the model.
summary.aov(fit)

#' The P vals for our predeictor are the same in each summary table, but the first summary command gives us more information that is regularly used in the literature, ie: effect size (slope) etc.
#'
#' ## Cautionary regression without point 4
#' Due to possible high leverage of this point in Cook's distance plot it's worth checking if there is a difference in coefficients. There is no change in this case so it's best to keep the full model.
# refit the model without row 4
fit_without_4 <- update(fit, data = sealice[-4])
# Model summary
summary(fit_without_4)

#' ## Visualising Regression
#' Plot comparisons using basic `abline()`, or predicted response variables to create confidence envelope using `matlines()`; a useful looking function for drawing multiple lines but not as eas for confidence envelopes as `visreg()`.
# basic plot
plot(
  GROWTH ~ INFECTION, data = sealice
  )
abline(fit)
# with confidence?
# x vals to predict y for using our model
newx <- seq(1,12,length=100)
# predicted response variables, with CIs
newy <- predict(
  fit, # model to use for prediction
  newdata = list(
    INFECTION = newx # where to find new predictor variables
  ),
  int = "c" # use 95% CI
)
plot(
  newx, newy[,1],
  xlab = "Infection Intensity (copepods/salmon)",
  ylab = "Predicted Growth Rate (g/experimental period)",
  col = "white"
)
# add 95% confidence envelope
matlines(
  newx, newy,
  lty = c("solid","dotted","dotted"),
  col = 1
)

