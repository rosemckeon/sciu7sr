# Luc Bussiere
# SCIU7SR & ENMPG03 regression clinic
# Oct 9 2012

# last modified Sept 29 2017 (by LMB)
#

# clear workspace
rm(list=ls())


# read data in frame and examine it
CG<-read.csv("Cairngorm2012.csv")
str(CG)
head(CG)

# explore distributions of responses and predictors for today's exercises
hist(CG$ELEVATION)

# whoa! we're not in the himalayas
# find troublesome entry
CG[CG$ELEVATION>2000,]
# remove troublesome entry from new data.frame
CGFIX<-CG[-37,]
# check data
hist(CGFIX$ELEVATION)
# better

hist(CGFIX$AIR.TEMP.CORR)
hist(CGFIX$SOIL.TEMP.CORR)

# elevation has a broadly uniform distribution, but it is a predictor, so this doesn't matter
# temp vars don't look terrible

# exploratory plots of how elevation affects both soil temp and air temp
par(mfrow=c(1,2))
plot(CGFIX$AIR.TEMP.CORR~CGFIX$ELEVATION)
plot(CGFIX$SOIL.TEMP.CORR~CGFIX$ELEVATION)
par(mfrow=c(1,1))

# to compare more effectively, make sure the y range is the same across both plots
par(mfrow=c(1,2))
plot(CGFIX$AIR.TEMP.CORR~CGFIX$ELEVATION,ylim=c(-6,7))
plot(CGFIX$SOIL.TEMP.CORR~CGFIX$ELEVATION,ylim=c(-6,7))
par(mfrow=c(1,1))

# there is a quite noticeable negative effect in both cases, but the effect is much stronger for AIR than for SOIL. Use the formula rise/run to make a good guess at the slope parameter.
# you may need to include the x=0 value to get a good guess at the y-intercept


# the null hypotheses should sound something like the following:
# H0 for AIR: There is no effect of altitude on air temperature
# or this
# H0 for SOIL: The slope parameter for the regression of soil temperature on elevation is zero.


# build consecutive models for air and soil temp

AIR.MOD<-lm(AIR.TEMP.CORR~ELEVATION,data=CGFIX)

# examine diagnostics
par(mfrow=c(2,2))
plot(AIR.MOD)
par(mfrow=c(1,1))

# Q-Q plot is not perfect (as there's a hint of non-normality in resids), but also not terrible -- looks OK

# view model summary
summary(AIR.MOD)



# For fun, let's see what would have happened if we had not cleaned the data

AIR.MOD.BAD<-lm(AIR.TEMP.CORR~ELEVATION,data=CG)

# examine diagnostics
par(mfrow=c(2,2))
plot(AIR.MOD.BAD)
par(mfrow=c(1,1))

# note the very odd behaviour of all four panels, and the clear high leverage and influence of record 37


# go back to correct model

# question 10 answers, briefly
# a negative
# b -0.019 degs change per m of altitude increase
# c conf limits around slope are 0.02129 and -0.01743 by hand
# you can also get these with the command below (although the numbers aren't exactly the same as the rough 1.96 SE)
confint(AIR.MOD)
# d I was right, of course
# e Yes, p<0.0001
# f 88.1%



# now soil
SOIL.MOD<-lm(SOIL.TEMP.CORR~ELEVATION,data=CGFIX)

# examine diagnostics
par(mfrow=c(2,2))
plot(SOIL.MOD)
par(mfrow=c(1,1))
# looks fine

# view model summary
summary(SOIL.MOD)

# question 10 answers, briefly
# a negative
# b -0.0068 degs change per m of altitude increase
confint(SOIL.MOD)
# c -0.0078 and -0.0057
# d I was right, of course, again
# e Yes, p<0.0001
# f 75.5%




# Sample Results text
# Both air temperature and soil temperature decreased at higher altitudes, although the effect in air was much stronger than it was in the soil (air slope = -0.0196 +- 0.0009, F(1,51)=443.2, P<0.0001; soil temperature slope = -0.0068 +- 0.0005, F(1,51)=156, P<0.0001; see Figure 1).




# prepare publication quality plots


# first, let's try making the plots using {graphics}
# generate new x vars over same range of x as data
summary(CGFIX)
NEWXVARS.AIR<-seq(650,1113,1)
# generate predictions of model
NEWYVARS.AIR<-predict(AIR.MOD,newdata=list(ELEVATION=NEWXVARS.AIR),int="c")

# same for SOIL
NEWXVARS.SOIL<-seq(650,1113,1)
# generate predictions of model
NEWYVARS.SOIL<-predict(SOIL.MOD,newdata=list(ELEVATION=NEWXVARS.SOIL),int="c")


# plot AIR
plot(CGFIX$AIR.TEMP.CORR~CGFIX$ELEVATION,xlab="Altitude (m)",ylab="Corrected Air Temperature (degs C)")
matlines(NEWXVARS.AIR,NEWYVARS.AIR,lty=c(1,2,2),col="black")

# plot soil
plot(CGFIX$SOIL.TEMP.CORR~CGFIX$ELEVATION,xlab="Altitude (m)",ylab="Corrected Soil Temperature (degs C)")
matlines(NEWXVARS.SOIL,NEWYVARS.SOIL,lty=c(1,2,2),col="black")

# if we want, we can position the two plots together rather easily
# note that you'll want to set the y-axis range to make sure they match across plots!

par(mfrow=c(1,2))
plot(CGFIX$AIR.TEMP.CORR~CGFIX$ELEVATION,xlab="Altitude (m)",ylab="Corrected Air Temperature (degs C)",main="Air Temperature",ylim=c(-6,6))
matlines(NEWXVARS.AIR,NEWYVARS.AIR,lty=c(1,2,2),col="black")
plot(CGFIX$SOIL.TEMP.CORR~CGFIX$ELEVATION,xlab="Altitude (m)",ylab="Corrected Soil Temperature (degs C)",main="Soil Temperature",ylim=c(-6,6))
matlines(NEWXVARS.SOIL,NEWYVARS.SOIL,lty=c(1,2,2),col="black")
par(mfrow=c(1,1))







# Anscombe's quartet


# the data frame anscombe already exists as part of R's datasets library, which is automatically loaded when you start R

# you can learn about it by passing the following line to the console
?anscombe

# the code at the bottom of this help file exploits loops to plo6t and fit models on all four series, but let's use more familiar (and less efficient) coding instead

# view the data frame and summary
anscombe
summary(anscombe)
# NB the x and y coordinates series are not adjacent, but that's no problem, obviously

# make four plots
par(mfrow=c(2,2))
plot(y1~x1,data=anscombe,xlim=c(3,20),ylim=c(0,16))
plot(y2~x2,data=anscombe,xlim=c(3,20),ylim=c(0,16))
plot(y3~x3,data=anscombe,xlim=c(3,20),ylim=c(0,16))
plot(y4~x4,data=anscombe,xlim=c(3,20),ylim=c(0,16))
par(mfrow=c(1,1))


# now build models and observe differences in the diagnostics as well as similarities in the coefficients
lm1<-lm(y1~x1,data=anscombe)
par(mfrow=c(2,2))
plot(lm1)
par(mfrow=c(1,1))
summary(lm1)

lm2<-lm(y2~x2,data=anscombe)
par(mfrow=c(2,2))
plot(lm2)
par(mfrow=c(1,1))
summary(lm2)

lm3<-lm(y3~x3,data=anscombe)
par(mfrow=c(2,2))
plot(lm3)
par(mfrow=c(1,1))
summary(lm3)

lm4<-lm(y4~x4,data=anscombe)
par(mfrow=c(2,2))
plot(lm4)
par(mfrow=c(1,1))
summary(lm4)

# Note that even panel four is not helpful in this case. Make sure you always plot your data as well as examining diagnostics


# can add regression lines to plot above if you want; here just fitted line (no confidence limits
par(mfrow=c(2,2))
plot(y1~x1,data=anscombe,xlim=c(0,20),ylim=c(0,16))
abline(lm1)
plot(y2~x2,data=anscombe,xlim=c(0,20),ylim=c(0,16))
abline(lm2)
plot(y3~x3,data=anscombe,xlim=c(0,20),ylim=c(0,16))
abline(lm3)
plot(y4~x4,data=anscombe,xlim=c(0,20),ylim=c(0,16))
abline(lm4)
par(mfrow=c(1,1))





