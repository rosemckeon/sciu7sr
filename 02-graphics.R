# Rose McKeon
# Visualising Data
# Stats with R, Autumn 2018

# clear the workspace
rm(list=ls())
# load dependencies
library(tidyverse)
source("lib/functions.R")

# These data are from a study of total floral richness as well
# as the species richness for a subset of plants that are characteristic
# of mature woodlands: the Ancient Woodland Vascular Plants, here
# abbreviated AWVP. In addition, the scientist who collected these data
# has included a quadrat identifier, the treatment applied to the quadrat,
# and two covariates that might affect species diversity:
# altitude and distance to established woodlands (both measured in m).
flowers <- read.csv("data/TLDataENMP03v2.csv")

# histograms of numerical data
hist(flowers$TREATMENT)
hist(flowers$FLORAL.RICHNESS)
hist(flowers$AWVP) # zero-inflation
hist(flowers$ALTITUDE) # mistake at high altitude likely
hist(flowers$DISTTOWOODS) # zero-inflation

# cleanup data -------------------------------------

# treatments as factors
flowers$TREATMENT <- as.factor(flowers$TREATMENT)
# erroneous altitude reset as NA
flowers <- flowers %>% mutate(
  ALTITUDE = replace(ALTITUDE, which(ALTITUDE > 2500), NA)
  )
hist(flowers$ALTITUDE)

# transformations ----------------------------------

# default graphical params
par_default <- par()
# setup multifigure panels for easy comparison...
par(mfrow=c(2,2))
# transformations for floral richness
hist(flowers$FLORAL.RICHNESS) # raw data
hist(sqrt(flowers$FLORAL.RICHNESS)) # square root
hist(log(flowers$FLORAL.RICHNESS)) # natural log - best?
hist(log10(flowers$FLORAL.RICHNESS)) # log base 10 - best?
# transformations for AWVP
hist(flowers$AWVP) # raw data
hist(sqrt(flowers$AWVP)) # square root
hist(log(flowers$AWVP)) # natural log - best?
hist(log10(flowers$AWVP)) # log base 10
# reset graphical params
# par(mfrow=c(1,1))
par(par_default)


# playing with histograms --------------------------
hist(
  flowers$FLORAL.RICHNESS,
  breaks = 20,
  xlab = "Floral Richness",
  freq = F,
  ylim = c(0,.1),
  main = "",
  col = "red"
  )

# plotting the relationships ------------------------

# compare the effectiveness of several planting regimes (encoded in
# TREATMENT) on the richness of flora, and especially of AWVP flora,
# in the plots. The ~ sign in the call can be read aloud as “depends on”, and is
# used to break apart the “dependent” part of the relationship.

# treatment effect on floral richness
plot(FLORAL.RICHNESS ~ TREATMENT, data = flowers)
# if treatments aren't seen as factors, less useful...
plot(FLORAL.RICHNESS ~ as.numeric(TREATMENT), data = flowers)

# treatment effect on ancient woodland vascular plant species
plot(AWVP ~ TREATMENT, data = flowers)
# with jitter to see more info about treatment 2
plot(AWVP ~ jitter(as.numeric(TREATMENT)), data = flowers)

# altitude effect on floral richness
plot(FLORAL.RICHNESS ~ ALTITUDE, data = flowers)
# with solid, coloured points indicating treatment
plot(
  FLORAL.RICHNESS ~ ALTITUDE,
  data = flowers,
  col = rgba(as.numeric(TREATMENT), .6),
  pch = 19
  )

# separately illustrate the effects of ALTITUDE and DISTTOWOODS on FLORAL.RICHNESS and AWVP
# (while simultaneously demonstrating the effects of TREATMENT in each figure)

# setup multifgure plot panel...
par(mfrow=c(2,2))
# distance to woods effect on floral richness
plot(
  FLORAL.RICHNESS ~ DISTTOWOODS,
  data = flowers,
  col = rgba(as.numeric(TREATMENT), .6),
  pch = 19
)
# altitude effect on floral richness
plot(
  FLORAL.RICHNESS ~ ALTITUDE,
  data = flowers,
  col = rgba(as.numeric(TREATMENT), .6),
  pch = 19
)
# distance to woods effect on ancient woodland vascular plant species
plot(
  AWVP ~ DISTTOWOODS,
  data = flowers,
  col = rgba(as.numeric(TREATMENT), .6),
  pch = 19
)
# altitude effect on ancient woodland vasculr plant species
plot(
  AWVP ~ ALTITUDE,
  data = flowers,
  col = rgba(as.numeric(TREATMENT), .6),
  pch = 19
)
# reset graphical params
par(par_default)

# A shorter distance to the nearest woodland seems to increase floral richness and tree diversity
# but it's not clear if this effect is more due to the treatment as all the points
# which are far from the woods and lower in floral richness/tree diversity are also all from one treatment.
# A similar relationship is apparent for altitude. Here, however, the treatment is less
# of a confounding factor, but there is a lack of data for higher altitudes.

# distance to woods appears to be collinear with treatment - this obscures our
# ability to distinguish the effects of the predictor on the response variable.
# The same is roughly (though less) true for altitude - some overlap and odd points out..

# examining the treatments ---------------------------

treatment1 <- flowers %>% filter(TREATMENT == "1")
treatment2 <- flowers %>% filter(TREATMENT == "2")
treatment3 <- flowers %>% filter(TREATMENT == "3")

# initiate plot with...
# distance to woods effect on floral richness in treatment 1
plot(
  FLORAL.RICHNESS ~ DISTTOWOODS,
  data = treatment1,
  col = rgba("red", .5),
  pch = "1",
  # make room for the full range of datapoints
  xlim = range(flowers$DISTTOWOODS),
  ylim = range(flowers$FLORAL.RICHNESS)
)
# add treatment 2
points(
  treatment2$DISTTOWOODS,
  treatment2$FLORAL.RICHNESS,
  col = rgba("green", .5),
  pch = "2"
)
# and treatment 3
points(
  treatment3$DISTTOWOODS,
  treatment3$FLORAL.RICHNESS,
  col = rgba("blue", .5),
  pch = "3"
)
# legend
key <- legend(
  2000, 37.5,
  legend = c("","",""), # empty labels re-written with text()
  col = rgba(c("red", "green", "blue"), .5),
  pch = c("1","2","3"),
  bty = "n",
  trace = T # allows us to position labels with text()
)
text(
  key$text$x-70,
  key$text$y-.1,
  rep("Treatment", 3),
  pos = 2
)

