# Rose McKeon
# Intro to R and rStudio
# September 2018

# required packages
library(tidyverse)

# simple maths
a <- 35
b <- 27
d <- a + b

# clearing the environment
rm(list=ls())

# get the data
worms <- read.csv("data/worms.csv")

# getting a column (area)
worms.area <- worms[,2] # no row index (ie ALL rows) for column 2
worms.area <- worms$AREA

# the tidyverse way... is not equivalent
# %>% is a 'pipe' works just like unix pipes (|) - stiches commands together
# in this instance it allows us to set worms as the base for select to run within
worms.area <- worms %>% select(AREA)

# -------------------
# indexing

worms$AREA[10] # the 10th record of AREA column
worms[10,2] # the 10th record of 2nd colum (same as above)

# and from the tidyverse...
# worms.area[10] # doesn't work as the structure of worms.area the tidyverse way is different.
worms.area$AREA[10]

# getting more columns
# we have no base R way of requesting multiple columns by name
new.worms <- worms[,2:4] # no row index (ALL rows) for columns 2 to 4.
new.worms <- worms[, c(2,3,4)] # the same, but now we could cherry pick columns that aren't sequential

# the tidyverse way brings back readable naming...
new.worms <- worms %>% select(AREA, SLOPE, VEGETATION)
# and this time all three methods are equivalent

# ----------------
# Filtering data

# whole table, but include only rows where AREA > 3
worms[worms$AREA > 3,]
# with tidyverse (also equivalent this time)
worms %>% filter(AREA > 3)

# with multiple conditions
worms[worms$AREA > 3 & worms$SLOPE < 3,]
# in tidyverse
worms %>% filter(AREA > 3 & SLOPE < 3)

# -----------------
# Sorting

# order columns 1-5 by column 2 (AREA) using nested function order()
worms[order(worms[,2]),1:5]
# with the tidyverse
worms %>% select(1:5) %>% arrange(AREA)
worms %>% arrange(desc(SOIL.PH))


# -------------
# Practice

damp <- worms %>% filter(DAMP)
meadows <- worms %>% filter(VEGETATION == "Meadow")
grasslands <- worms %>% filter(VEGETATION == "Grassland") %>% arrange(WORM.DENSITY)
low_density <- worms %>% filter(WORM.DENSITY < 5.5) %>% arrange(SLOPE)
nongrassland_area_ph <- worms %>% filter(VEGETATION != "Grassland") %>% select(AREA, SOIL.PH)



