# Rose McKeon
# Visualising Data, supplement
# Stats with R, Autumn 2018

# clear the workspace
rm(list=ls())
# load dependencies
library(tidyverse)
source("lib/functions.R")
par_default <- par() # default plot style
par(bty = "l") # plot box type

# get the data (response = BIOMASS)
worms <- read.csv("data/worms2.csv")

# check the data
hist(worms$BIOMASS)
hist(worms$SLOPE)
hist(worms$SOIL.PH)
hist(worms$WORM.DENSITY)

# examine possible outliers
worms %>% filter(SOIL.PH > 9)
worms %>% filter(FIELD.NAME == "Mistake.Record")

# cleanup data
worms <- worms %>% filter(FIELD.NAME != "Mistake.Record")

# check the data
hist(worms$BIOMASS) # roughly normal
hist(worms$SLOPE) # skewed
hist(worms$SOIL.PH) # nearly normal
hist(worms$WORM.DENSITY) # skewed

# transformations ---------------------
# setup the plotting pane for 2x2 plots
par(mfrow = c(2,2))
# transform slope
hist(worms$SLOPE)
hist(sqrt(worms$SLOPE))
hist(log(worms$SLOPE))
hist(log10(worms$SLOPE)) # most normal
# store best trnasformation
worms$SLOPE.log10 <- log10(worms$SLOPE)
# transform worm density
hist(worms$WORM.DENSITY)
hist(sqrt(worms$WORM.DENSITY))
hist(log(worms$WORM.DENSITY)) # most normal
hist(log10(worms$WORM.DENSITY))
# store best transformation
worms$WORM.DENSITY.log <- log(worms$WORM.DENSITY)
# return plotting pane to 1x1
par(mfrow = c(1,1))


# relationships -----------------------
# vegetation type effect on biomass
boxplot(
  BIOMASS ~ VEGETATION,
  data = worms,
  col = rgba(c(2:6),.3),
  xlab = "Vegetation Type",
  ylab = "Biomass"
  )

# slope effect on biomass
plot(
  BIOMASS ~ SLOPE,
  data = worms
)
# fit linear model
fit <- lm(BIOMASS ~ SLOPE, data = worms)
abline(fit) # line of best fit
abline(mean(worms$BIOMASS), 0, lty = 2) # null hypothesis (for comparison)

# slope effect on biomass depending on damp
# A relationship between slope and biomass is apparent only under damp
# conditions. When it is damp, biomass is reduced when slope is greater.
# What does this imply about the effect of slope on area?
# creat subsets using damp condition
damp <- worms %>% filter(DAMP == T)
not_damp <- worms %>% filter(DAMP == F)
# base plot with damp points
par(
  lwd = 2,
  las = 1,
  cex = 1.2,
  mar = c(5,5,2,2)
)
plot(
  BIOMASS ~ SLOPE,
  data = damp,
  pch = 19,
  col = rgba("blue", .3),
  xlim = range(worms$SLOPE),
  ylim = range(worms$BIOMASS)
)
# not damp points
points(
  not_damp$SLOPE,
  not_damp$BIOMASS,
  pch = 19,
  col = rgba("red", .3)
)
# fit the linear models for subsets
fit_damp <- lm(BIOMASS ~ SLOPE, data = damp)
fit_not_damp <- lm(BIOMASS ~ SLOPE, data = not_damp)
# plot the lines
abline(fit_damp, col = rgba("blue", .3), lwd = 2)
abline(fit_not_damp, col = rgba("red", .3), lwd = 2)
#legend
legend(
  "topright",
  legend = c("Damp","Not Damp"),
  col = rgba(c("blue", "red"), .3),
  pch = 19,
  bty = "n"
)
